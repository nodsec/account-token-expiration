export default Settings = function () {

    // Default expiration interval = 5min
    const DEFAULT_EXPIRATION_INTERVAL = 5 * 60 * 1000;

    // Default expire on close browser window/tab
    const DEFAULT_EXPIRE_ON_CLOSE_WINDOW = false;

    // Default heardBeat interval = 30s
    const DEFAULT_HEART_BEAT_INTERVAL = 5 * 1000;

    // Default disabled is set to false
    const DEFAULT_ENABLED = false;

    // Default activity events
    const DEFAULT_ACTIVITY_EVENTS = 'click mousemove keydown DOMMouseScroll mousewheel mousedown touchstart touchmove focus';

    let settings = {
        expirationInterval: DEFAULT_EXPIRATION_INTERVAL,
        expireOnCloseWindow: DEFAULT_EXPIRE_ON_CLOSE_WINDOW,
        heartBeatInterval: DEFAULT_HEART_BEAT_INTERVAL,
        activityEvents: DEFAULT_ACTIVITY_EVENTS,
        enabled: DEFAULT_ENABLED
    }

    let refreshSettings = function () {
        settings.enabled = (
            Meteor.settings &&
            Meteor.settings.public &&
            Meteor.settings.public.SESSION_EXPIRATION &&
            Meteor.settings.public.SESSION_EXPIRATION.enabled !== null) ?
            Meteor.settings.public.SESSION_EXPIRATION.enabled : DEFAULT_ENABLED;

        settings.expirationInterval =
            Meteor.settings &&
            Meteor.settings.public &&
            Meteor.settings.public.SESSION_EXPIRATION &&
            Meteor.settings.public.SESSION_EXPIRATION.expirationInterval || DEFAULT_EXPIRATION_INTERVAL;

        settings.expireOnCloseWindow =
            (Meteor.settings &&
                Meteor.settings.public &&
                Meteor.settings.public.SESSION_EXPIRATION &&
                Meteor.settings.public.SESSION_EXPIRATION.expireOnCloseWindow !== null) ? Meteor.settings.public.SESSION_EXPIRATION.expireOnCloseWindow : DEFAULT_EXPIRE_ON_CLOSE_WINDOW;

        settings.heartBeatInterval =
            Meteor.settings &&
            Meteor.settings.public &&
            Meteor.settings.public.SESSION_EXPIRATION &&
            Meteor.settings.public.SESSION_EXPIRATION.heartBeatInterval || DEFAULT_HEART_BEAT_INTERVAL;

        settings.activityEvents = (
            Meteor.settings &&
            Meteor.settings.public &&
            Meteor.settings.public.SESSION_EXPIRATION &&
            Meteor.settings.public.SESSION_EXPIRATION.activityEvents) ?
            Meteor.settings.public.SESSION_EXPIRATION.activityEvents : DEFAULT_ACTIVITY_EVENTS;
    }

    let getSettings = function () {
        refreshSettings();
        return settings;
    }

    return {
        getSettings,
        refreshSettings,
        settings
    }
}();
