export default Settings = function() {

    // Default expiration interval = 5min
    const DEFAULT_EXPIRATION_INTERVAL = 5 * 60 * 1000;

    // Default active logs are disabled
    const DEFAULT_ACTIVE_LOGS = false;

    // Default heardBeat interval = 30s
    const DEFAULT_HEART_BEAT_INTERVAL = 5 * 1000;

    // Default disabled is set to false
    const DEFAULT_ENABLED = true;

    let settings = {
        expirationInterval: DEFAULT_EXPIRATION_INTERVAL,
        heartBeatInterval: DEFAULT_HEART_BEAT_INTERVAL,
        activeLogs: DEFAULT_ACTIVE_LOGS,
        enabled: DEFAULT_ENABLED
    }

    let refreshSettings = function() {
        // Default disabled is set to false
        settings.enabled = (
            Meteor.settings &&
            Meteor.settings &&
            Meteor.settings.SESSION_EXPIRATION &&
            Meteor.settings.SESSION_EXPIRATION.enabled !== null) ?
            Meteor.settings.SESSION_EXPIRATION.enabled : DEFAULT_ENABLED;

        // Default expiration interval = 5min
        settings.expirationInterval =
            Meteor.settings &&
            Meteor.settings.SESSION_EXPIRATION &&
            Meteor.settings.SESSION_EXPIRATION.expirationInterval || DEFAULT_EXPIRATION_INTERVAL;

        // Default heardBeat interval = 60s
        settings.heartBeatInterval =
            Meteor.settings &&
            Meteor.settings.SESSION_EXPIRATION &&
            Meteor.settings.SESSION_EXPIRATION.heartBeatInterval || DEFAULT_HEART_BEAT_INTERVAL;

        // Show active console logs, default = false
        settings.activeLogs =
            Meteor.settings &&
            Meteor.settings.SESSION_EXPIRATION &&
            Meteor.settings.SESSION_EXPIRATION.activeLogs !== null ? Meteor.settings.SESSION_EXPIRATION.activeLogs : DEFAULT_ACTIVE_LOGS;
    }

    let getSettings = function() {
        refreshSettings();
        return settings;
    }

    return {
        getSettings,
        refreshSettings,
        settings
    }
}();
